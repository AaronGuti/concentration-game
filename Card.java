package concentrationGame;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * @author Aaron Gutierrez Card class which contains behaviors and attributes
 *         that allow each individual card to properly react to user interaction
 *
 */
public class Card extends JButton {
	// Properties
	private Color faceColor;
	private static Color backColor = Color.GRAY;

	private boolean isFaceDown = true; // all cards are faced down upon game start
	private boolean matched = false;
	private boolean disabled = false;

	// Constructor
	public Card(Color faceColor, int width, int height) {
		this.faceColor = faceColor;
		setPreferredSize(new Dimension(width, height));
		setBorder(BorderFactory.createLineBorder(Color.black, 4));
		faceDown();
	}

	// Behaviors
	/**
	 * The faceUp method takes no parameters and allows the card object to flip over
	 * to reveal its face.
	 */
	public void faceUp() {
		isFaceDown = false;
		changeColor(faceColor);
	}

	/**
	 * The faceDown method does not take any parameters. It flips the card object
	 * back over to reveal the back of the card.
	 */
	public void faceDown() {
		isFaceDown = true;
		changeColor(backColor);
	}

	/**
	 * The changeColor class take a color as a parameter and changes the color of
	 * the card .
	 * 
	 * @param color
	 *            a color fed by a user or set by another method.
	 */
	public void changeColor(Color color) {
		setBackground(color);
		repaint();
	}

	/**
	 * The equals method overrides the superclass equals method. It tests to see if
	 * the face colors of the passed cards are equal.
	 * 
	 * @param other
	 *            the card the original card will be tested against
	 * @return returns a boolean true or false depending on if the card faces are
	 *         equivalent
	 */
	public boolean equals(Card other) {
		Card otherCard = (Card) other;
		return (faceColor.equals(otherCard.faceColor));

	}

	/**
	 * 
	 */
	public void setDisabled() {
		this.disabled = true;
	}

	/**
	 * Checks to see if a card is in fact faceDown
	 * 
	 * @return a boolean true or false
	 */
	public boolean isFaceDown() {
		return isFaceDown;
	}

	/**
	 * 
	 * @return
	 */
	public Color getColor() {
		return faceColor;
	}

	/**
	 * Sets the status of a card to match if it has found its pair
	 * 
	 * @param matched
	 *            boolean true/false if the card has been matched or not
	 */
	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	/**
	 * Informs whether or not the card has been matched
	 * 
	 * @return true/false if matched
	 */
	public boolean getMatched() {
		return matched;
	}
}