package concentrationGame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.Timer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

/**
 * GameBoard sets the playing field for the concentration game.
 * 
 * @author Aaron Gutierrez
 */
public class GameBoard extends JFrame {
	private int numberofCards;
	private ArrayList<Card> cardsList = new ArrayList<>();
	private Color[] colors = { Color.BLUE, Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.MAGENTA };
	private int boardWidth = 700;
	private int boardHeight = 700;
	private int pairs;
	private Timer t;

	private JPanel statusBar;
	private JLabel statusLabel;

	public static void main(String[] args) {
		new GameBoard(10);
	}

	// Constructors
	/**
	 * Constructor for the game board creates the game board by using the number of
	 * cards to create a grid for them to be placed on. The cards are then dealed in
	 * a random fashion so all there is no outstanding pattern.
	 * 
	 * @param numberOfPairs
	 *            an integer to be entered and doubled to get the actual amount of
	 *            cards
	 */
	public GameBoard(int numberOfPairs) {
		numberofCards = numberOfPairs * 2;
		pairs = numberOfPairs;

		setTitle("Concentration Game");
		setSize(boardWidth, boardHeight);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Container c = getContentPane();
		c.setLayout(new GridLayout(findFactors(numberOfPairs), 0));

		GameController gc = new GameController();

		// Assemble and display cards
		assignColors(numberOfPairs);
		Collections.shuffle(cardsList);

		for (Card card : cardsList) {
			c.add(card);
			card.addActionListener((ActionListener) gc);
		}

		statusBar = createStatusBar();
		c.add(statusBar, BorderLayout.NORTH);
		setVisible(true);

	}

	/**
	 * Deals out the collors in a random fahion as well as create the cards within a
	 * reasonable size. Takes account how many pairs there will be in relevance to
	 * the game board size. Adds the cards to an array of cards for later use.
	 * 
	 * @param numberOfPairs
	 *            number of pairs of caeds
	 */
	public void assignColors(int numberOfPairs) {
		Random randomizer = new Random();
		while (numberOfPairs > 0) {
			if (numberOfPairs <= 6) {
				for (Color color : colors) {
					Card c1 = new Card(color, boardWidth / pairs, boardHeight / pairs);
					Card c2 = new Card(color, boardWidth / pairs, boardHeight / pairs);
					cardsList.add(c1);
					cardsList.add(c2);
					numberOfPairs = numberOfPairs - 1;

				}
			}
			if (numberOfPairs > 6) {
				Color aColor = colors[randomizer.nextInt(6)];
				Card c1 = new Card(aColor, boardWidth / pairs, boardHeight / pairs);
				Card c2 = new Card(aColor, boardWidth / pairs, boardHeight / pairs);
				cardsList.add(c1);
				cardsList.add(c2);
				numberOfPairs = numberOfPairs - 1;
			}
		}
	}

	/**
	 * Creates a nice little welcome panel at the bottom of the board.
	 * 
	 * @return the welcome panel
	 */
	private JPanel createStatusBar() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		statusLabel = new JLabel("Welcome to the Concentration Game!");
		panel.add(statusLabel, BorderLayout.NORTH);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED));

		return panel;
	}

	/**
	 * Find the best dimensions for the Grid Layout
	 * 
	 * @param numberOfPairs
	 *            number of cards
	 * @return an integer that best represents the way the grid should be laid out
	 */
	private int findFactors(int numberOfPairs) {
		int number = numberOfPairs;
		double use = Math.sqrt(number);
		return (int) Math.ceil(use) + 1;
	}

	/**
	 * Checks to see if the game is won by seeing if all the card's statuses are set
	 * to matched, if not then the game is not won.
	 * 
	 * @return boolean regarding the finished/unfinished state of the game
	 */
	public boolean Winner() {
		for (Card c : this.cardsList) {
			if (c.getMatched() == false) {
				return false;
			}
		}
		return true;

	}

	/**
	 * Game Controller reacts to user interaction by flipping cards and having a
	 * timer to allow the user to see a cards location before they are flipped back
	 * over if they do not match.
	 * 
	 * @author Aaron Gutierrez
	 *
	 */
	private class GameController implements ActionListener {
		private int moves;
		private ArrayList<Card> activeCards = new ArrayList<>();

		public void actionPerformed(ActionEvent argO) {
			moves++;
			t = new Timer(1500, new ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (activeCards.size() == 2) {
						if (activeCards.get(0).equals(activeCards.get(1)) == false) {
							activeCards.get(0).faceDown();
							activeCards.get(1).faceDown();
							activeCards.clear();
						} else if (activeCards.get(0).equals(activeCards.get(1)) == true) {
							activeCards.get(0).setEnabled(false);
							activeCards.get(1).setEnabled(false);
							activeCards.get(0).setMatched(true);
							activeCards.get(1).setMatched(true);
							activeCards.clear();
						}
					}
					if (Winner()) {
						System.out.println("Congrats! You won in " + moves + " moves!");
						System.exit(0);
					}
				}
			});

			Object aCard = argO.getSource();
			Card clickedCard = (Card) aCard;
			activeCards.add(clickedCard);
			clickedCard.setDisabled();
			clickedCard.faceUp();
			t.start();

		}

	}
}
